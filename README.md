# HW №1
Для обеспечения качества кода мы используем pre-commit хуки. Убедитесь, что вы установили все зависимости из `requirements.txt` и запустили `pre-commit install` перед началом работы.
# HW №2
Собрать докер:
`docker build -t mlops .`

Запуск:
`docker run mlops`
