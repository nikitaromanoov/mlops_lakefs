FROM postgres:13

# Установка необходимых зависимостей
RUN apt-get update \
    && apt-get install -y wget \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Установка lakeFS
RUN wget https://github.com/treeverse/lakeFS/releases/download/v1.20.0/lakeFS_1.20.0_Linux_arm64.tar.gz
RUN ls
RUN tar -xzvf lakeFS_1.20.0_Linux_arm64.tar.gz
RUN ls
RUN mv lakefs /bin/lakefs

# Копирование конфигурационного файла и скриптов инициализации
COPY ./config.yaml /lakefs/config.yaml
COPY ./init_lakefs.sh /docker-entrypoint-initdb.d/

# Открываем порт 8000 для доступа к UI и API lakeFS
EXPOSE 8000

# Установка точки входа
ENTRYPOINT ["lakefs"]
CMD ["run"]